#include <stdio.h>
#include <stdlib.h>
#include "estudiantes.h"
#include <math.h>

int i=0;           //Se define la variable aca arriba, ya que se ocupara en todas las funciones
float promedio;    //Se define la variable aca arriba, ya que see ocupara en mas de una variable
		
void desvStd(estudiante curso[]){   //Se define la funcion desviacion estandar
	float media = 0, media2, varianza = 0,desviacion=0 ; //Se definen las variables a ocupar
	for (int i = 0; i < 24; ++i){    
		media += curso[i].prom; //se establece la suma de todos los promedios con la operacion "+=" y se almacena en la variable media
		}
		media2=(media)/24.0; // se divide lo obtenido en 24, ya que es el numero de datos y se almacena en la variable media2
		printf("La Media Aritmetica es: %.1f\n", media2); //Se imprime el resultado
	for (i = 0; i < 24; i++){
		varianza += (curso[i].prom - media2)*(curso[i].prom - media2); //Se saca el cuadrado de cada promedio menos la media
		}
	varianza=(varianza)/23.0; //se divide el resultado en la cantidad de datos menos 1
	printf("La varianza es: %.1f\n",varianza ); //Se imprime el resultado
	desviacion = sqrt(varianza); //Con la funcion "sqrt" se saca  la raiz cuadrada al resultado obtenido antes
	printf("La desviación estandar es: %.1f\n", desviacion); //Se imprime el resultado
}

void menor(estudiante curso[]){ //Se define la funcion del menor promedio
	float menor = curso[0].prom; //Se define la variable, que vale el promedio del primer estudiante
	for (i = 0; i < 24; ++i){ //Se establece el ciclo "for"

		if (curso[i].prom < menor){  //Si el promedio del siguiente es menor al anterior 
			menor = curso[i].prom; //El menor toma el lugar del otro
				
		}
	}

	printf("el promedio menor es: %.1f\n",menor ); //Se imprime el resultado
}

void mayor(estudiante curso[]){  //Se define la funcion del mayor promedio
	float mayor = curso[0].prom;  //Se define la variable, que vale el promedio del primer estudiante
	for (i = 0; i < 24; ++i){   //Se establece el ciclo "for"

		if (curso[i].prom > mayor){ //Si el promedio del siguiente es menor al anterior
			mayor = curso[i].prom; //El menor toma el lugar del otro
				
		}
	}

	printf("el promedio mayor es: %.1f\n",mayor ); //Se imprime el resultado

}

void registroCurso(estudiante curso[]){ //Se define la funcion registroCurso
 
	for (i=0;i<24;i++){
		printf("%s %s %s\n",curso[i].nombre , curso[i].apellidoP, curso[i].apellidoM); //Se imprime el nombre y los apellidos del alumno
		printf("Proyecto 1: \n");                    //Se pide agregar cada una de las notas
		scanf("%f",&curso[i].asig_1.proy1);
		printf("Proyecto 2: \n");
		scanf("%f",&curso[i].asig_1.proy2);
		printf("Proyecto 3: \n");
		scanf("%f",&curso[i].asig_1.proy3);
		printf("control 1: \n");
		scanf("%f",&curso[i].asig_1.cont1);
		printf("control 2: \n");
		scanf("%f",&curso[i].asig_1.cont2);
		printf("control 3: \n");
		scanf("%f",&curso[i].asig_1.cont3);
		printf("control 4: \n");
		scanf("%f",&curso[i].asig_1.cont4);
		printf("control 5: \n");
		scanf("%f",&curso[i].asig_1.cont5);
		printf("control 6: \n");
		scanf("%f",&curso[i].asig_1.cont6);
		promedio=curso[i].asig_1.proy1*0.2+curso[i].asig_1.proy2*0.2+curso[i].asig_1.proy3*0.3+curso[i].asig_1.cont1*0.05+curso[i].asig_1.cont2*0.05+curso[i].asig_1.cont3*0.05+curso[i].asig_1.cont4*0.05+curso[i].asig_1.cont5*0.05+curso[i].asig_1.cont6*0.05; //Se saca el promedio
		printf("El promedio es:%.1f \n",promedio); //Se imprime el resultado
		curso[i].prom = promedio; //Se almacena la variable promedio en la estructura curso[i].prom
			
	}

}

void clasificarEstudiantes(char aprobadosp[], char reprobadosp[], estudiante curso[]){
	FILE *aprobados; 
	FILE *reprobados;
	if((aprobados=fopen(aprobadosp,"w"))==NULL){ //Se crea el archivo aprobados
		printf("\nerror al crear el archivo");
		exit(0); 
	}
	else{
		for (int i = 0; i<24; i++){
			if (curso[i].prom>=4.0) { //Si el promedio es 4.0 se va a aprobados
				fprintf(aprobados, "%s %s %s %.1f \n", curso[i].nombre, curso[i].apellidoP, curso[i].apellidoM,curso[i].prom ); //Si el promedio es mayor a 4 se le agregan los datos a aprobados
			}
		}
	} 
	printf("\n **El registro de estudiantes aprobados fue almacenado**\n \n ");
	fclose(aprobados);

	if((reprobados=fopen(reprobadosp,"w"))==NULL){ 
		printf("\nerror al crear el archivo");
		exit(0); 
	}
	else{
		for (int i = 0; i<24; i++){
			if (curso[i].prom<=3.9){ //Si el promedio es 4.0 se va a aprobados 
				fprintf(reprobados, "%s %s %s %.1f \n", curso[i].nombre, curso[i].apellidoP, curso[i].apellidoM,curso[i].prom ); //Si el promedio es mayor a 4 se le agregan los datos a aprobados
			}
		}
	} 
	printf("\n **El registro de estudiantes reprobados fue almacenado**\n \n ");
	fclose(aprobados);

}


void metricasEstudiantes(estudiante curso[]){ //Se ejecuta la funcion mayor que contiene las otras funciones mas pequeñas
	menor(curso);
	mayor(curso);
	desvStd(curso);
}





void menu(estudiante curso[]){
	int opcion;
    do{
		printf( "\n   1. Cargar Estudiantes" );
		printf( "\n   2. Ingresar notas" );
		printf( "\n   3. Mostrar Promedios" );
		printf( "\n   4. Almacenar en archivo" );
		printf( "\n   5. Clasificar Estudiantes " );
		printf( "\n   6. Salir." );
		printf( "\n\n   Introduzca opción (1-6): " );
		scanf( "%d", &opcion );
        switch ( opcion ){
			case 1: cargarEstudiantes("estudiantes.txt", curso); //carga en lote una lista en un arreglo de registros
					break;

			case 2:  registroCurso(curso);// Realiza ingreso de notas en el registro curso



					break;

			case 3: metricasEstudiantes(curso); //presenta métricas de disperción, tales como mejor promedio; más bajo; desviación estándard.
					
					break;

			case 4: grabarEstudiantes("test.txt",curso);
					break;
            case 5: clasificarEstudiantes("aprobados.txt", "reprobados.txt", curso); // clasi
            		break;
         }

    } while ( opcion != 6 );
}

int main(){
	estudiante curso[30]; 
	menu(curso); 
	return EXIT_SUCCESS; 
}